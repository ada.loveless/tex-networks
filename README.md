# Line maps in TeX

Some line maps I drew in TeX

## Heidelberg

<img src="https://gitlab.com/ada.loveless/tex-networks/-/jobs/artifacts/main/raw/rnv/heidelberg.svg?job=build-svg" alt="A network map of the Heidelberg Tram network" width=100%></img>

## Mannheim 

<img src="https://gitlab.com/ada.loveless/tex-networks/-/jobs/artifacts/main/raw/rnv/mannheim.svg?job=build-svg" alt="A network map of the Mannheim Tram network" width=100%></img>

## RNV 5 as a circle

<img src="https://gitlab.com/ada.loveless/tex-networks/-/jobs/artifacts/main/raw/rnv/disc-five.svg?job=build-svg" alt="A circular map of the RNV 5" width=100%></img>

## Freiburg

<img src="https://gitlab.com/ada.loveless/tex-networks/-/jobs/artifacts/main/raw/vag_freiburg/freiburg.svg?job=build-svg" alt="A network map of the Freiburg Tram network" width=100%></img>

## S-Bahn Rhein-Neckar

<img src="https://gitlab.com/ada.loveless/tex-networks/-/jobs/artifacts/main/raw/db_regio_mitte/s-bahn-rhein-neckar.svg?job=build-svg" alt="A network map of the S-Bahn Rhein-Neckar" width=100%></img>
